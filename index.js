module.exports = function (shipit) {
  // extend shipit with additional methods defined in the shipit-deploy module
  require('lodash').assign(shipit.constructor.prototype, require('shipit-deploy/lib/shipit'));

  require('./lib/sync-repo')(shipit);
  require('./lib/print-latest-tags')(shipit);
  require('./lib/version')(shipit);
  require('./lib/push-new-version')(shipit);
};
