# Shipit-deploy-package-support
Set of tasks for deploying packaged apps using [shipit](https://github.com/shipitjs/shipit) and the [shipit-deploy](https://github.com/shipitjs/shipit-deploy) plugin.
Provides a set of task that can be integrated with Shipit's `deploy` taks.

## Tasks
- `deploy:printLatestTags` - prints the last N (default: 5) tags
- `deploy:version` - queries the user to enter the version(tag) that should be deployed
- `deploy:syncRepo` - synchronizes the remote repository with your release repository
- `deploy:pushNewVersion` - fetches the specified version from the synchronized repository and deploys it to the remote server (test, staging)

## Installation
```bash
npm install shipit-deploy-package-support
```

## Configuration
Inside your `shipitfile.js` add the following configuration:
```js
// if you want to print the last 10 tags
shipit.numberOfTagsToPrint = 10;

shipit.packagedDeployConfig = {
  syncUser: 'myUser',
  syncRepositoryUrl: 'sync-repo.url.com',
  pathToSyncScript: '/path/to/the/sync/script.sh',

  packageName: 'myApp',
  packageFileExtension: 'tar.gz',
  packageExtractionCommand: 'tar xzf',
  pathToPackageRepository: '/path/to/the/release/repository'
};
```


## Example `shipitfile.js`
```js
module.exports = function (shipit) {
  shipit.packagedDeployConfig = {
    // ....
  };

  shipit.initConfig({
    test: {
      // ...
    },
    production: {
      // ...
    }
  });

  // Override of the default deploy task
  shipit.task('deploy', [
    'deploy:init',
    'deploy:syncRepo',
    'deploy:printLatestTags',
    'deploy:version',
    'deploy:pushNewVersion',
    'deploy:publish',
    'deploy:clean',
    'deploy:finish'
  ]);
```
