module.exports = function (shipit) {
  var Promise = require('bluebird');
  var Readline = require('readline');

  shipit.blTask('deploy:version', task);

  function task() {
    return new Promise(function (resolve) {
      var rl = Readline.createInterface(process.stdin, process.stdout);
      rl.question('Version to deploy:', function (version) {
        shipit.versionToDeploy = version;
        rl.close();
        resolve(shipit.versionToDeploy);
      });
    });
  }
};
