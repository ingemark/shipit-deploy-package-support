module.exports = function (shipit) {
  var Moment = require('moment');
  var Path = require('path2/posix');
  var Util = require('util');
  var Promise = require('bluebird');

  shipit.blTask('deploy:pushNewVersion', task);

  function task() {
    if (!shipit.packagedDeployConfig) {
      return Promise.reject('Missing configuration for deploying a packaged app');
    }
    if (!shipit.versionToDeploy) {
      return Promise.reject('App version not specified');
    }

    return createReleasePath()
      .then(deployNewVersion)
      .then(setCurrentRevision);
  }

  function createReleasePath() {
    shipit.releaseDirname = Moment.utc().format('YYYYMMDDHHmmss');
    shipit.releasePath = Path.join(shipit.releasesPath, shipit.releaseDirname);

    shipit.log('Creating the release path "%s"', shipit.releasePath);
    return shipit.remote('mkdir -p ' + shipit.releasePath)
      .then(function () {
        shipit.log('Release path created.');
      });
  }

  function deployNewVersion() {
    // example: myApp-0.1.2.tar.gz
    var packageFile = Util.format(
      '%s-%s.%s',
      shipit.packagedDeployConfig.packageName,
      shipit.versionToDeploy,
      shipit.packagedDeployConfig.packageFileExtension
    );

    // example: sync-machine.com/path/to/repo/myApp/0.1.2/myApp-0.1.2.tar.gz
    var packageUrl = Util.format(
      '%s/%s/%s/%s/%s',
      shipit.packagedDeployConfig.syncRepositoryUrl,
      shipit.packagedDeployConfig.pathToPackageRepository,
      shipit.packagedDeployConfig.packageName,
      shipit.versionToDeploy, packageFile
    );

    shipit
      .remote(Util.format('cd %s && wget -q %s', shipit.releasePath, packageUrl))
      .then(function () {
        return shipit.remote(Util.format(
          'cd %s && %s %s',
          shipit.releasePath,
          shipit.packagedDeployConfig.packageExtractionCommand,
          packageFile));
      })
      .then(function () {
        return shipit.remote(Util.format('cd %s && rm %s', shipit.releasePath, packageFile));
      });
  }

  function setCurrentRevision() {
    shipit.log('Setting current revision and creating revision file.');
    shipit.currentRevision = shipit.versionToDeploy;
    return shipit
      .remote('echo "' + shipit.currentRevision + '" > ' + Path.join(shipit.releasePath, 'REVISION'))
      .then(function () {
        shipit.log('Revision file created.');
      });
  }
};
