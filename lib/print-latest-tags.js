module.exports = function (shipit) {
  shipit.blTask('deploy:printLatestTags', task);

  function task() {
    shipit.numberOfTagsToPrint = shipit.numberOfTagsToPrint || 5;
    return shipit.local(
      "git for-each-ref refs/tags --format='%(refname:short)' --sort=-taggerdate --count="
      + shipit.numberOfTagsToPrint
    );
  }
};
