module.exports = function (shipit) {
  var ChildProcess = require('child_process');
  var Util = require('util');
  var Promise = require('bluebird');

  shipit.blTask('deploy:syncRepo', task);

  function task() {
    if (!shipit.packagedDeployConfig) {
      return Promise.reject('Sync configuration missing');
    }

    shipit.log('Syncing the remote repository with our repository...');
    ChildProcess.execSync(
      Util.format(
        'ssh %s@%s "bash -c \'%s\'"',
        shipit.packagedDeployConfig.syncUser,
        shipit.packagedDeployConfig.syncRepositoryUrl,
        shipit.packagedDeployConfig.pathToSyncScript
      ),
      // the child process should use the parents stdin, stdout, stderr
      {stdio: [0, 1, 2]},
      function (error, stdout, stderr) {
        if (error !== null) {
          shipit.log('An error occurred while synchronizing repositories', error);
          reject(error);
        } else {
          shipit.log('Repositories successfully synchronized');
          shipit.emit('deploy:syncedRepo');
        }
      }
    );
  }
};
